import { useState, useEffect } from 'react'
import {useAppContext} from '../context/appContext';
import { Logo, FormRow, Alert } from '../components'
import Wrapper from '../assets/wrappers/RegisterPage'

import { useNavigate } from 'react-router-dom'
const initialState = {
  name: "",
  email: "",
  password: "",
  isMember: false,

};

const Register = () => {
  const navigate = useNavigate()
  const [values, setValues] = useState(initialState)
  const { user,isLoading, showAlert, displayAlert, registerUser,loginUser } = useAppContext();
  const toggleMember = () => {
    setValues({ ...values, isMember: !values.isMember });
  };


  const handleChange = (e) => {
   setValues({ ...values, [e.target.name]: [e.target.value] });
  }
  const onSubmit = (e) => {
    e.preventDefault()
    const { name, email, password, isMember } = values
    console.log(name);
    if (!email || !password || (!isMember && !name)) {
      displayAlert()
      return
    }
    const currentUser = { name, email, password }
    if (isMember) {
      loginUser(currentUser)
      // setupUser({
      //   currentUser,
      //   endPoint: 'login',
      //   alertText: 'Login Successful! Redirecting...',
      // })
    } else {
      registerUser(currentUser);
      // setupUser({
      //   currentUser,
      //   endPoint: 'register',
      //   alertText: 'User Created! Redirecting...',
      // })
    }
  }

  useEffect(() => {
    if (user) {
      setTimeout(() => {
        navigate('/')
      }, 3000)
    }
  }, [user, navigate])

  return (
    <Wrapper className="full-page">
      <form className="form" onSubmit={onSubmit}>
        <Logo />
        <h3>{values.isMember ? "Login" : "Register"}</h3>
        {showAlert && <Alert />}
        {!values.isMember && (
          <FormRow
            type="text"
            name="name"
            value={values.name}
            handleChange={handleChange}
          />
        )}
        {/* email input */}
        <FormRow
          type="email"
          name="email"
          value={values.email}
          handleChange={handleChange}
        />
        {/* password input */}
        <FormRow
          type="password"
          name="password"
          value={values.password}
          handleChange={handleChange}
        />
        <button type="Submit" className="btn btn-block" disabled={isLoading}>
          Submit
        </button>
                <p>
          {values.isMember ? 'Not a member yet?' : 'Already a member?'}
          <button type='button' onClick={toggleMember} className='member-btn'>
            {values.isMember ? 'Register' : 'Login'}
          </button>
        </p>
      </form>
    </Wrapper>
  );
}
export default Register
