import React , {useState,useReducer,useContext} from 'react';
import reducer from "./reducer";
import axios from 'axios'
import {
  DISPLAY_ALERT,
  CLEAR_ALERT,
  SETUP_USER_BEGIN,
  SETUP_USER_SUCCESS,
  SETUP_USER_ERROR,
  TOGGLE_SIDEBAR,
  LOGOUT_USER,
  UPDATE_USER_BEGIN,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_ERROR,
  HANDLE_CHANGE,
  CLEAR_VALUES,
  CREATE_JOB_BEGIN,
  CREATE_JOB_SUCCESS,
  CREATE_JOB_ERROR,
  GET_JOBS_BEGIN,
  GET_JOBS_SUCCESS,
  SET_EDIT_JOB,
  DELETE_JOB_BEGIN,
  EDIT_JOB_BEGIN,
  EDIT_JOB_SUCCESS,
  EDIT_JOB_ERROR,
  SHOW_STATS_BEGIN,
  SHOW_STATS_SUCCESS,
  CLEAR_FILTERS,
  CHANGE_PAGE,
  REGISTER_USER_BEGIN,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
  LOGIN_USER_BEGIN,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
} from "./actions";
const token = localStorage.getItem("token");
const user = localStorage.getItem("user");
const userLocation = localStorage.getItem("location");

const initialState = {
  isLoading: false,
  showAlert: false,
  alertText: "",
  alertType: "",
  user: user ? JSON.parse(user) : null,
  token: token,
  userLocation: userLocation || "",
  showSidebar:false
};

const AppContext = React.createContext()

const AppProvider = ({children}) => {

const [state, dispatch] = useReducer(reducer, initialState);
  const displayAlert = () => {
    dispatch({ type: DISPLAY_ALERT });
    clearAlert();
  };
  const addUserToLocalStorage = ({ user, token, location }) => {
    localStorage.setItem("user", JSON.stringify(user));
    localStorage.setItem("token", token);
    localStorage.setItem("location", location);
  };
  const removeUserFromLocalStorage = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    localStorage.removeItem('location')
  }
    const clearAlert = () => {
          setTimeout(() => {
            dispatch({ type: CLEAR_ALERT });
          }, 3000);
        };
    const registerUser = async (currentUser) => {
      console.log(currentUser.name[0]);
      const sendPayload = {
        name: currentUser.name[0],
        email: currentUser.email[0],
        password: currentUser.password[0],
      };
        dispatch({ type: REGISTER_USER_BEGIN })
        try {
          const response = await axios.post(
            "api/v1/auth/register",
            sendPayload
          );
          console.log(response);
          const { user, token, location } = response.data;
          dispatch({
            type: REGISTER_USER_SUCCESS,
            payload: {
              user,
              token,
              location,
              alertText: "User Created! Redirecting...",
            },
          });
                addUserToLocalStorage({ user, token, location });
        } catch (error) {
          dispatch({
            type: REGISTER_USER_ERROR,
            payload: { msg: error.response.data.msg },
          });
        }
      };

      const loginUser = async(currentUser)=>{
              const sendPayload = {
                email: currentUser.email[0],
                password: currentUser.password[0],
              };
              dispatch({ type: LOGIN_USER_BEGIN });
              try {
                const response = await axios.post(
                  "api/v1/auth/login",
                  sendPayload
                );
                console.log(response);
                const { user, token, location } = response.data;
                dispatch({
                  type: LOGIN_USER_SUCCESS,
                  payload: {
                    user,
                    token,
                    location,
                    alertText: "User LOGIN! Redirecting...",
                  },
                });
                addUserToLocalStorage({ user, token, location });
              } catch (error) {
                dispatch({
                  type: LOGIN_USER_ERROR,
                  payload: { msg: error.response.data.msg },
                });
              }
      }
  const toggleSidebar = () => {
    dispatch({ type: TOGGLE_SIDEBAR });
  };
    const logoutUser = () => {
      dispatch({ type: LOGOUT_USER });
      removeUserFromLocalStorage();
    };
    return (
      <AppContext.Provider
        value={{
          ...state,
          displayAlert,
          registerUser,
          loginUser,
          toggleSidebar,
          logoutUser,
        }}
      >
        {children}
      </AppContext.Provider>
    );
}

const useAppContext = () =>{
    return useContext(AppContext)
}
export { AppProvider, initialState, useAppContext };